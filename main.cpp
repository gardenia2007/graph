/*
 * =====================================================================================
 *
 *       Filename:  main.cpp
 *
 *    Description:  Graph
 *
 *        Version:  1.0
 *        Created:  2011年12月07日 20时48分15秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  李洪祥
 *        Company:
 *
 * =====================================================================================
 */


/*
1. 已知图的邻接矩阵表示，求其邻接表表示；或相反。
2. 已知有向图的正邻接表表示，求其逆邻接表表示；或相反。
3. 分别以邻接矩阵和邻接表作为存储结构，设计算法计算有向图中入度/出度为零的顶点个数。
4. 基于深度优先搜索/广度优先搜索编写算法，判断以邻接表/邻接矩阵存储的有向图中是否存在由顶点 vi 到顶点 vj 的路径（i≠j）。
5. 采用邻接矩阵和邻接表存储结构，实现 Prim 或 Kruskal 算法。
6．实现关键路径问题的算法。(未完成）
 */



#include <iostream>
#include <stdio.h>
#include <fstream>
using namespace std;
#define NUM_OF_VERTICES 32
typedef int VertexDataType;
typedef int EdgeDataType;
#define MAXLENGTH 128
#define MAXCOST 0x7fffffff
#define START_FIND 0
#define HAS_FIND_Vi 1
#define HAS_FIND_Vj 2
#define DONE -1

/**********无向图和有向图的邻接矩阵存储结构的建立 ************/

typedef struct {
    VertexDataType vertexlist[NUM_OF_VERTICES];
    EdgeDataType edge[NUM_OF_VERTICES][NUM_OF_VERTICES];
    int n, e; //图的顶点和边数
} MTGraph;

void CreateMGraphOrder(MTGraph *G) {
    fstream fin;
    fin.open("graphMO.txt", ios::in);
    if (fin.fail()) {
        cout << "Open File Error!";
    }
    int i, j, w;
    fin >> G->n >> G->e; //输入顶点数和边数
    for (i = 0; i < G->n; i++)
        fin >> G->vertexlist[i];
    for (i = 0; i < G->n; i++)
        for (j = 0; j < G->n; j++)
            G->edge[i][j] = 0; //初始化
    for (int k = 0; k < G->e; k++) {
        fin >> i >> j >> w; //读入各边是否连通，连通输入“1”，不连通输入“0”
        G->edge[i][j] = w;
    }
    fin.close();
}

/**********无向图和有向图的邻接表存储结构的建立 ************/

typedef struct node {//边表结点
    int adjvex; //邻接点域(下标)
    EdgeDataType cost; //边上的权值
    struct node *next; //下一边链接指针
} EdgeNode;

typedef struct {//顶点表结点
    VertexDataType vertex; //顶点数据域
    EdgeNode * firstedge; //边链表头指针
} VertexNode;

typedef struct {//图的邻接表
    VertexNode vexlist [NUM_OF_VERTICES];
    int n, e; //顶点个数与边数
} AdjGraph;

void CreateGraphListOrder(AdjGraph *G) {
    ifstream fin;
    fin.open("graphLO.txt");

    int tail, head, weight;
    fin >> G->n >> G->e; //1.输入顶点个数和边数
    for (int i = 0; i < G->n; i++) {//2.建立顶点表
        fin >> G->vexlist[i].vertex; //2.1输入顶点信息
        G->vexlist[i].firstedge = NULL; //2.2边表置为空表
    }
    for (int k = 0; k < G->e; k++) {//3.逐条边输入,建立边表
        fin >> head >> tail >> weight; //3.1输入
        EdgeNode * p = new EdgeNode; //3.2建立边结点
        p->adjvex = tail;
        p->cost = weight; //3.3设置边结点
        p->next = G->vexlist[head].firstedge; //3.4链入第 head 号链表的前端
        G->vexlist[head].firstedge = p;
    }
}

void HomeWork_1_MTGraphToAdjGraph(MTGraph *P, AdjGraph *G) {
    G->n = P->n;
    G->e = P->e;

    for (int i = 0; i < G->n; i++) {//2.建立顶点表
        G->vexlist[i].vertex = P->vertexlist[i]; //2.1输入顶点信息
        G->vexlist[i].firstedge = NULL; //2.2边表置为空表
    }

    for (int i = 0; i < G->n; i++) {//3.逐个添加边
        for (int j = 0; j < G->n; j++) {
            if (P->edge[i][j] != 0) {
                EdgeNode * g = new EdgeNode; //3.2建立边结点
                g->adjvex = j;
                g->cost = P->edge[i][j]; //3.3设置边结点
                g->next = G->vexlist[i].firstedge; //3.4链入第 head 号链表的前端
                G->vexlist[i].firstedge = g;
            }
        }
    }
}

void HomeWork_2_List(AdjGraph *G, AdjGraph *P) {
    for (int i = 0; i < G->n; i++) {//建立顶点表
        P->vexlist[i].vertex = G->vexlist[i].vertex; //输入顶点信息
        P->vexlist[i].firstedge = NULL; //边表置为空表
    }

    for (int i = 0; i < G->n; i++) {
        EdgeNode *q = G->vexlist[i].firstedge;
        while (q) {
            int head = q->adjvex;
            EdgeNode * g = new EdgeNode; //建立边结点
            g->adjvex = i;
            g->cost = q->cost; //设置边结点
            g->next = G->vexlist[head].firstedge; //链入第 head 号链表的前端
            P->vexlist[head].firstedge = g;
            q = q->next;
        }
    }
}

void HomeWork_3_MTGraph(MTGraph *G) {
    int in_cnt = 0;
    int out_cnt = 0;
    bool flag = false;
    for (int i = 0; i < G->n; i++) {
        flag = false;
        for (int j = 0; j < G->n; j++) {
            if (G->edge[i][j] != 0) {
                flag = true;
                break;
            }
        }
        if (!flag)
            out_cnt++;
    }
    for (int i = 0; i < G->n; i++) {
        flag = false;
        for (int j = 0; j < G->n; j++) {
            if (G->edge[j][i] != 0) {
                flag = true;
                break;
            }
        }
        if (!flag)
            in_cnt++;
    }
    cout << "Out :" << out_cnt << "\nIn :" << in_cnt << endl;
}

void HomeWork_3_AdjGraph(AdjGraph *G) {
    int in_cnt = 0;
    int out_cnt = 0;
    for (int i = 0; i < G->n; i++) {//出度为零的顶点
        if (!G->vexlist[i].firstedge)
            out_cnt++;
    }

    char flag[NUM_OF_VERTICES] = {'0'}; //入度为零的顶点
    for (int i = 0; i < G->n; i++) {
        EdgeNode *p = G->vexlist[i].firstedge;
        while (p) {
            flag[p->adjvex] = '1';
            p = p->next;
        }
    }
    for (int i = 0; i < G->n; i++) {
        if (flag[i] == '0')
            in_cnt++;
    }
    cout << "Out :" << out_cnt << "\nIn :" << in_cnt << endl;

}

bool visited[NUM_OF_VERTICES]; //访问标记数组
int status, vi, vj;

void L_DFS(AdjGraph *G, int i) {
    visited[i] = true;
    if (status == START_FIND && G->vexlist[i].vertex == vi)
        status = HAS_FIND_Vi;
    else if (status == HAS_FIND_Vi && G->vexlist[i].vertex == vj)
        status = HAS_FIND_Vj;
    else if (status == HAS_FIND_Vj)
        return;
    EdgeNode *p;
    p = G->vexlist[i].firstedge;
    while (p) {
        if (!visited[p->adjvex]) {
            L_DFS(G, p->adjvex);
        }
        p = p->next;
    }
}

void HomeWork_4_AdjGraph(AdjGraph *G) {//邻接表深度优先遍历
    cout << "Input vi(vi->vj):";
    cin >> vi;
    cout << "Input vj(vi->vj):";
    cin >> vj;
    int i;
    status = START_FIND;
    for (i = 0; i < G->n; i++)//标记数组初始化
        visited[i] = false;
    for (i = 0; i < G->n; i++) {
        if (status == HAS_FIND_Vj) {
            cout << "YES!\n";
            return;
        } else
            status = START_FIND;
        if (!visited[i])
            L_DFS(G, i);
    }
    cout << "NO!\n";
}

void M_DFS(MTGraph *G, int i) {
    visited[i] = true;
    if (status == HAS_FIND_Vj)
        return;
    else if (status == HAS_FIND_Vi && G->vertexlist[i] == vj)
        status = HAS_FIND_Vj;
    else if (status == START_FIND && G->vertexlist[i] == vi)
        status = HAS_FIND_Vi;
    for (int j = 0; j < G->n; j++) {
        if (G->edge[i][j] != 0 && !visited[j]) {
            M_DFS(G, j);
        }
    }
}

void HomeWork_4_MTGraph(MTGraph *G) {
    cout << "Input vi(vi->vj):";
    cin >> vi;
    cout << "Input vj(vi->vj):";
    cin >> vj;
    int i;
    status = START_FIND;
    for (i = 0; i < G->n; i++)//标记数组初始化
        visited[i] = false;
    for (i = 0; i < G->n; i++) {
        if (status == HAS_FIND_Vj) {
            cout << "YES!\n";
            return;
        } else
            status = START_FIND;
        if (!visited[i])
            M_DFS(G, i);
    }
    cout << "NO!\n";
}

void HomeWork_5_Prim_MTGraph(MTGraph *G) {
    // lowcost[i]记录以i为终点的边的最小权值，当lowcost=DONE时表示终点i加入生成树  
    int lowcost[MAXLENGTH];
    // mst[i]记录对应lowcost[i]的起点，当mst[i]=0时表示起点i加入生成树  
    int mst[MAXLENGTH];
    int i, j, min, minid, sum = 0;
    // 默认选择0号节点加入生成树，从1号节点开始初始化  
    for (i = 1; i < G->n; i++) {
        // 最短距离初始化为其他节点到0号节点的距离  
        if (G->edge[0][i] == 0)
            lowcost[i] = MAXCOST;
        else
            lowcost[i] = G->edge[0][i];
        // 标记所有节点的起点皆为默认的0号节点  
        mst[i] = 0;
    }
    // 标记0号节点加入生成树  
    mst[0] = 0;
    lowcost[0] = DONE;
    // n个节点至少需要n-1条边构成最小生成树  
    for (i = 1; i < G->n; i++) {
        min = MAXCOST;
        minid = 0;
        // 找满足条件的最小权值边的节点minid  
        for (j = 1; j < G->n; j++) {
            // 边权值较小且不在生成树中  
            if (lowcost[j] < min && lowcost[j] != DONE) {
                min = lowcost[j];
                minid = j;
            }
        }
        // 输出生成树边的信息:起点，终点，权值  
        cout << G->vertexlist[mst[minid]] << " - " << G->vertexlist[minid] << " : " << min << endl;
        // 累加权值  
        sum += min;
        // 标记节点minid加入生成树  
        lowcost[minid] = DONE;
        // 更新当前节点minid到其他节点的权值  
        for (j = 1; j < G->n; j++) {
            // 发现更小的权值  
            if (G->edge[minid][j] < lowcost[j] && lowcost[j] != DONE && G->edge[minid][j] != 0) {
                // 更新权值信息  
                lowcost[j] = G->edge[minid][j];
                // 更新最小权值边的起点
                mst[j] = minid;
            }
        }
    }
    // 返回最小权值和  
    cout << "Cost: " << sum << endl;
}

void HomeWork_5_Prim_AdjGraph(AdjGraph *G) {
    // lowcost[i]记录以i为终点的边的最小权值，当lowcost=DONE时表示终点i加入生成树  
    int *lowcost = new int[G->n];
    for (int i = 0; i < G->n; i++)
        lowcost[i] = MAXCOST;

    // mst[i]记录对应lowcost[i]的起点，当mst[i]=0时表示起点i加入生成树  
    int *mst = new int[G->n];
    int i, j, min, minid, sum = 0;
    EdgeNode *p;
    // 默认选择0号节点加入生成树，从1号节点开始初始化  
    p = G->vexlist[0].firstedge;
    while (p) {
        // 最短距离初始化为其他节点到0号节点的距离  
        lowcost[p->adjvex] = p->cost;
        // 标记所有节点的起点皆为默认的0号节点  
        mst[p->adjvex] = 0;
        p = p->next;
    }
    // 标记0号节点加入生成树  
    mst[0] = 0;
    lowcost[0] = DONE;
    // n个节点至少需要n-1条边构成最小生成树  
    for (i = 1; i < G->n; i++) {
        min = MAXCOST;
        minid = 0;
        // 找满足条件的最小权值边的节点minid  
        for (j = 1; j < G->n; j++) {
            // 边权值较小且不在生成树中  
            if (lowcost[j] < min && lowcost[j] != DONE) {
                min = lowcost[j];
                minid = j;
            }
        }
        // 输出生成树边的信息:起点，终点，权值  
        cout << G->vexlist[mst[minid]].vertex << " - " << G->vexlist[minid].vertex << " : " << min << endl;
        // 累加权值  
        sum += min;
        // 标记节点minid加入生成树  
        lowcost[minid] = DONE;
        // 更新当前节点minid到其他节点的权值  
        p = G->vexlist[minid].firstedge;
        while (p) {
            // 发现更小的权值  
            if (p->cost < lowcost[p->adjvex] && lowcost[p->adjvex] != DONE) {
                // 更新权值信息  
                lowcost[p->adjvex] = p->cost;
                // 更新最小权值边的起点
                mst[p->adjvex] = minid;
            }
            p = p->next;
        }
    }
    // 返回最小权值和  
    cout << "Cost: " << sum << endl;
}

int main() {
    cout << "\t\tGraph Test Program\n";

    MTGraph MTGraph; //有向图邻接矩阵
    AdjGraph AdjGraph, Test; //有向图邻接表


    CreateMGraphOrder(&MTGraph);
    cout << "**HomeWork 3 MTGraph:\n";
    HomeWork_3_MTGraph(&MTGraph);
    cout << "**HomeWork 4 MTGraph:\n";
    HomeWork_4_MTGraph(&MTGraph);
    cout << "**HomeWork 5 MTGraph:\n";
    HomeWork_5_Prim_MTGraph(&MTGraph);


    CreateGraphListOrder(&AdjGraph);
    cout << "**HomeWork 3 AdjGraph:\n";
    HomeWork_3_AdjGraph(&AdjGraph);
    cout << "**HomeWork 4 AdjGraph:\n";
    HomeWork_4_AdjGraph(&AdjGraph);
    cout << "**HomeWork 5 AdjGraph:\n";
    HomeWork_5_Prim_AdjGraph(&AdjGraph);

    cout << "**HomeWork 1:\n";
    HomeWork_1_MTGraphToAdjGraph(&MTGraph, &Test);
    HomeWork_5_Prim_AdjGraph(&Test);

    cout << "**HomeWork 2:\n";
    HomeWork_2_List(&AdjGraph, &Test);


    return 0;
}






